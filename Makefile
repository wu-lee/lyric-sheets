
.PHONY: all
all: Frozen1.pdf Frozen2.pdf


Frozen1.pdf: Frozen1.tex Frozen1/*.tex
	pdflatex $<

Frozen2.pdf: Frozen2.tex Frozen2/*.tex
	pdflatex $< 
