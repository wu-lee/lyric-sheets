#!/bin/bash

outdir=content

set -vx
while read -r line; do
    link=${line#*:}
    title=${line%%:*}
    title2="${title// /_}"
    echo "get $title2: $link"
    mkdir -p "$outdir"
    rm -f "$outdir"/*
    wget -O "$outdir/$title2.html" $link
    html2text -style pretty -utf8 -o "$outdir/$title2.txt"  "$outdir/$title2.html"
done <<EOF
All Frozen Lyrics: https://www.lyricsondemand.com/soundtracks/f/frozenlyrics/frozenalbumlyrics.html
All Frozen 2 Lyrics: https://www.lyricsondemand.com/soundtracks/f/frozeniilyrics/frozeniialbumlyrics.html
EOF
